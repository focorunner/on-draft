Rails.application.routes.draw do
  root                'static_pages#home'
  get    'help'    => 'static_pages#help'
  get    'about'   => 'static_pages#about'
  get    'contact' => 'static_pages#contact'
  get    'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :recipes
  resources :ingredients, except: [:show]
  resources :ferms
  resources :hops
  resources :yeasts

  # The following is how you route to share controllers/views
  # Note: You'll need separate folder and partial for each 'type' regardless
  #resources :ferms, controller: 'ingredients', type: 'ferm'
  #resources :hops, controller: 'ingredients', type: 'hop'
  #resources :yeasts, controller: 'ingredients', type: 'yeast'
end