class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@ondraft.com"
  layout 'mailer'
end
