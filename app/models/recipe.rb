class Recipe < ActiveRecord::Base
  has_many :rec_items, dependent: :destroy
  has_many :ingredients, through: :rec_items
end
