class Ingredient < ActiveRecord::Base
  has_many :rec_items
  has_many :recipes, through: :rec_items

end
