class HopsController < ApplicationController
  def index
    @hops = Hop.all.paginate(page: params[:page], per_page: 20).order(:name)
  end

  def show
    @hop = Hop.find(params[:id])
  end

  def new
    @hop = Hop.new
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
