class IngredientsController < ApplicationController
  def index
    @ingredients = Ingredient.all.paginate(page: params[:page], per_page: 20)
  end

  def show
  end

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
