class RecipesController < ApplicationController
  def index
    @recipes = Recipe.all.paginate(page: params[:page], per_page: 20).order(:name)
  end

  def show
    @recipe = Recipe.find(params[:id])
    @items = @recipe.rec_items.group_by { |ri| ri.ingredient.type }
  end

  def new
    @recipe = Recipe.new
  end

  def create
    @recipe = Recipe.new(recipe_params)
    if @recipe.save
      flash[:success] = "Recipe Created"
      redirect_to recipe_url
    else
      flash[:danger] = "Failed to Create Recipe"
      render 'new'
    end
  end

  def edit
    @recipe = Recipe.find(params[:id])
  end

  def update
    @recipe = Recipe.find(params[:id])
    if @recipe.save(recipe_params)
      flash[:success] = "Recipe Updated"
      redirect_to recipe_url
    else
      flash[:danger] = "Failed to Update Recipe"
      render 'edit'
    end
  end

  def destroy
    @recipe = Recipe.find(params[:id])
    @recipe.destroy
    redirect_to recipes_url
  end

  private
    def recipe_params
      params(:id).require(:recipe).permit(:name, :style, :batch_vol, :wort_vol,
        :est_srt, :est_ibu, :efficiency, :boil_duration, :evap_rate )
    end
end