class YeastsController < ApplicationController
  def index
    @yeasts = Yeast.all.paginate(page: params[:page], per_page: 20).order(:name)
  end

  def show
    @yeast = Yeast.find(params[:id])
  end

  def new
    @yeast = Yeast.new
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
