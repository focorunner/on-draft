class FermsController < ApplicationController
  def index
    @ferms = Ferm.all.paginate(page: params[:page], per_page: 20).order(:name)
  end

  def show
    @ferm = Ferm.find(params[:id])
  end

  def new
    @ferm = Ferm.new
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
