#create admin user
User.create!(name:  "Admin",
             email: "beer@focorunner.com",
             password:              "foobar",
             password_confirmation: "foobar",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

# Seed Recipes
Recipe.create!(name: "Super No. 1 Ale",
                style: "English Ale",
                batch_vol: 5.0,
                wort_vol: 5.0,
                est_srm: 5.0,
                est_ibu: 22.0,
                efficiency: 89.0,
                boil_duration: 60,
                evap_rate: 5)

Recipe.create!(name: "Fantastic Stout",
                style: "Irish Stout",
                batch_vol: 5.0,
                wort_vol: 5.0,
                est_srm: 22.0,
                est_ibu: 1.5,
                efficiency: 79.0,
                boil_duration: 90,
                evap_rate: 5)

# Seed Recipe Items
RecItem.create!(recipe_id: 1, ingredient_id: 58, amount: 7, unit: "lbs")
RecItem.create!(recipe_id: 1, ingredient_id: 10, amount: 1, unit: "lbs")
RecItem.create!(recipe_id: 1, ingredient_id: 24, amount: 0.5, unit: "lbs")
RecItem.create!(recipe_id: 1, ingredient_id: 98, amount: 2, unit: "oz", boil_time: 30)
RecItem.create!(recipe_id: 1, ingredient_id: 98, amount: 1, unit: "oz", boil_time: 10)
RecItem.create!(recipe_id: 1, ingredient_id: 172)

RecItem.create!(recipe_id: 2, ingredient_id: 55, amount: 9.5, unit: "lbs")
RecItem.create!(recipe_id: 2, ingredient_id: 12, amount: 1.0, unit: "lbs")
RecItem.create!(recipe_id: 2, ingredient_id: 13, amount: 2.0, unit: "lbs")
RecItem.create!(recipe_id: 2, ingredient_id: 105, amount: 2, unit: "oz", boil_time: 60)
RecItem.create!(recipe_id: 2, ingredient_id: 96, amount: 1, unit: "oz", boil_time: 10)
RecItem.create!(recipe_id: 2, ingredient_id: 261)

# Seed Fermentables from CSV
File.open('db/ingredients.csv') do |ingredients|
ingredients.read.each_line do |ingredient|
  type, name, origin, subtype, color, pot_sg, alpha, lab, strain, kind, state, floc =
    ingredient.chomp.split(",")
  Ingredient.create!( type: type,
                      name: name,
                      origin: origin,
                      subtype: subtype,
                      color: color,
                      pot_sg: pot_sg,
                      alpha: alpha,
                      lab: lab,
                      strain: strain,
                      kind: kind,
                      state: state,
                      floc: floc )
  end
end
