class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :name
      t.string :style
      t.float :batch_vol
      t.float :wort_vol
      t.float :est_srm
      t.float :est_ibu
      t.float :efficiency
      t.integer :boil_duration
      t.integer :evap_rate
      t.timestamps null: false
    end
  end
end
