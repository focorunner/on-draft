class CreateRecItems < ActiveRecord::Migration
  def change
    create_table :rec_items do |t|
      t.belongs_to :recipe, index: true
      t.belongs_to :ingredient, index: true
      t.float :amount
      t.string :unit
      t.integer :boil_time
      t.timestamps null: false
    end
  end
end
