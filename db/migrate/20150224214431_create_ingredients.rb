class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|
      t.string :type
      t.string :name
      t.string :origin
      t.string :subtype
      t.integer :color
      t.decimal :pot_sg, precision: 4, scale: 3
      t.decimal :alpha, precision: 3, scale: 1
      t.string :lab
      t.string :strain
      t.string :kind
      t.string :state
      t.string :floc
      t.float :atten
      t.timestamps null: false
    end
  end
end
