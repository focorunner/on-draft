# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150224215544) do

  create_table "ingredients", force: :cascade do |t|
    t.string   "type"
    t.string   "name"
    t.string   "origin"
    t.string   "subtype"
    t.integer  "color"
    t.decimal  "pot_sg",     precision: 4, scale: 3
    t.decimal  "alpha",      precision: 3, scale: 1
    t.string   "lab"
    t.string   "strain"
    t.string   "kind"
    t.string   "state"
    t.string   "floc"
    t.float    "atten"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "rec_items", force: :cascade do |t|
    t.integer  "recipe_id"
    t.integer  "ingredient_id"
    t.float    "amount"
    t.string   "unit"
    t.integer  "boil_time"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "rec_items", ["ingredient_id"], name: "index_rec_items_on_ingredient_id"
  add_index "rec_items", ["recipe_id"], name: "index_rec_items_on_recipe_id"

  create_table "recipes", force: :cascade do |t|
    t.string   "name"
    t.string   "style"
    t.float    "batch_vol"
    t.float    "wort_vol"
    t.float    "est_srm"
    t.float    "est_ibu"
    t.float    "efficiency"
    t.integer  "boil_duration"
    t.integer  "evap_rate"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
