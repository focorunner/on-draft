require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "On Draft"
    assert_equal full_title("Help"), "Help | On Draft"
  end
end